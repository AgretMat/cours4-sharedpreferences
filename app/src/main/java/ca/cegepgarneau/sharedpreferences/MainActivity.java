package ca.cegepgarneau.sharedpreferences;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //deux autres modes : MODE_WORLD_WRITTEABLE et MODE_WORLD_READABLE pour que les données soient modifiables/lisibles par d'autres apps.
        prefs = getSharedPreferences("MonFichierDeSauvegarde", MODE_PRIVATE);
        String texte = prefs.getString("texte", "Rien");

        TextView afficherExtraction = findViewById(R.id.lecture);
        afficherExtraction.setText(texte);
        Button boutonLecture = findViewById(R.id.lire);
        boutonLecture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String texte = prefs.getString("texte", "Rien");//Rien par défaut
                TextView afficherExtraction = findViewById(R.id.lecture);
                afficherExtraction.setText(texte);
            }
        });
        Button boutonEcrire = findViewById(R.id.sauvegarder);
        boutonEcrire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = prefs.edit(); //pour obtenir une version modifiable
                EditText ev = findViewById(R.id.infoASauvegarder);
                editor.putString("texte", ev.getText().toString());
                editor.apply();
            }
        });
    }
}
